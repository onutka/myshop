<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Product;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function __construct()
    { $this->middleware('auth')->except('index', 'show');


    }
    public function index(){
        /*$categories = Categorie::index();
        $categories['categories']=$categories;*/
        $categories=Categorie::all();
        $data['categories']=$categories;
        return view('categories', $data);
    }

    public function show(Categorie $categorie){

        return view('categories.show', compact('categorie'));
    }


}
