<?php

namespace App\Http\Controllers;
use App\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function  orders(){
        $orders=Order::all();
        $data['orders']=$orders;
        return view('orders', $data);
    }

    public function  show($id){
        $order=Order::find($id);
        $data['order']=$order;
        return view('orders.show', $data);
    }
}
