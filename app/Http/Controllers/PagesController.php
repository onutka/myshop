<?php
namespace App\Http\Controllers;

use App\Page;

use Illuminate\Http\Request;

class PagesController extends Controller
{   public function __construct()
    { $this->middleware('auth')->except('index');


    }

    public function index(){
    $pages = Page::all();
    $data['pages']=$pages;
    return view('pages', $data);
    }

    public function show(Page $page){


        return view('pages.show', compact('page'));

    }

    public function create(){
        return view('pages.create');
    }

    public function store(){
       /* request()->all();*/
        $this->validate(request(),[
            'title'=>'required|min:4|unique:pages,title',
            'alias'=>'required|min:4|max:30|unique:pages,alias',
            'intro'=>'required|min:1|max:600',
            'content'=>'required|min:10|max:10000',

        ]);
        Page::create(request()->all()); // короткий вариант с обязат заполнением fillable или protected в модели Page (ограничение безопасности)
        return redirect('/pages');
      /* $page= new Page();  // длинный вариант с явным указанием полей, используем когда названия полей не совпадают с названиями полей в табличке
       $page->title=request('title');
       $page->alias=request('alias');
       $page->intro=request('intro');
       $page->content=request('content');
       $page->save();
       return redirect('/');*/
    }
    public function edit(Page $page){
       return view('pages.edit', compact('page'));
    }

    public function update(Page $page){
        $this->validate(request(),[
            'title'=>'required|min:4|unique:pages,title,'.$page->id,
            'alias'=>'required|min:4|max:30|unique:pages,alias,'. $page->id,
            'intro'=>'required|min:1|max:600',
            'content'=>'required|min:10|max:10000',

        ]);
        $page->update(request(['title', 'alias', 'intro', 'content']));
        return redirect('/pages');
    }
    public function delete (Page $page){
        return view('pages.delete', compact('page'));
    }

    public function destroy(Page $page){
        $page->delete();
        return redirect('/pages');

    }
}
