<?php
namespace App\Http\Controllers;
use App\Categorie;
use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function  index(){
        $products=Product::all();
        $data['products']=$products;
        return view('products', $data);
    }

    public function  show(Product $product){

        return view('products.show', compact('product'));
    }
    public function create(){
        return view('products.create');
    }

    public function store(){
        /* request()->all();*/
    // короткий вариант , но нужно обязательно заполнить допущ. cв-во fillable в модели Product
        $this->validate(request(),[
            'title'=>'required|min:4|unique:products,title',
            'alias'=>'required|min:4|max:30|unique:pages,alias',
            'description'=>'required|min:10|max:1000',
            'price'=>'required|min:1|max:6',
        ]);

        Product::create(request()->all());
        return redirect('/products');

       /*$product= new Product();  // Более длинный и явно прописанный вариант
        $product->title=request('title');
        $product->alias=request('alias');
        $product->description=request('description');
        $product->price=request('price');
        $product->save();
        */
    }

    public function edit(Product $product){
        return view('products.edit', compact('product'));
    }

    public function update(Product $product){
        $this->validate(request(),[
        'title'=>'required|min:4|unique:products,title,'.$product->id,
        'alias'=>'required|min:4|max:30|unique:products,alias,'.$product->id,
        'description'=>'required|min:10|max:1000',
        'price'=>'required|min:1|max:6',
    ]);
        $product->update(request(['title', 'alias', 'description', 'price']));
        return redirect('/products');

    }
    public function delete (Product $product){
        return view('products.delete', compact('product'));
    }

    public function destroy(Product $product){
        $product->delete();
        return redirect('/products');

    }




}
