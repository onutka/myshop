<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    public function create()
    {
        return view('users.create');
    }

    public function store()
    {
        $this->validate(request(),
            ['name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'min:6|required',
                'password_confirmation' => 'min:6|same:password'

            ]);

        User::create(
            ['name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
        ]);
        return redirect('/');
    }
}
