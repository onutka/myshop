<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=['title','alias','description','price'];
    public function getRouteKeyName()
    {
        return 'alias';
    }
    public function product(){

        return $this->belongsTo(Categorie::class);

//        return $this->hasMany(Comment::class,'post_id','id');

    }
}
