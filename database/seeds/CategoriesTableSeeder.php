<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['title'=>'Столы',
                'alias'=>'stoli',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['title'=>'Стулья',
                'alias'=>'stulya',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['title'=>'Диваны и кресла',
                'alias'=>'divany-kresla',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['title'=>'Кровати',
                'alias'=>'krovati',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['title'=>'Шкафы',
                'alias'=>'shkafy',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['title'=>'Другие изделия',
                'alias'=>'drugoe',
                'created_at'=>date('Y-m-d H:i:s')
            ]
        ]);
    }
}
