<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            ['customer_name'=>'tatyanka77',
                'email'=>'tatyanka77@gmail.com',
                'phone'=>'0502223322',
                'feedback'=>'Все классно, закажу еще',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['customer_name'=>'mikhail',
                'email'=>'michael@ukr.net',
                'phone'=>'050232322',
                'feedback'=>'Отличный сервис, доставили очень быстро',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['customer_name'=>'andrey',
                'email'=>'andrey1988@ukr.net',
                'phone'=>'0675552299',
                'feedback'=>'Посоветовали с выбором, хорошее соотношение цены-качества',
                'created_at'=>date('Y-m-d H:i:s')
            ]
        ]);
    }
}
