<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            ['title'=>'Кресло, Хольмби',
                'alias'=>'kreslo-holmbi',
                'description'=>'Каркас подлокотника: Березовый шпон, Прозрачный акриловый лак
Трубчатый каркас: Сталь',
                'price'=>'1350',
                'category_id'=>'3',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['title'=>'Кресло, темно-синий Gubbo',
                'alias'=>'kreslo-gubbo',
                'description'=>'Каркас сиденья: ДСП, Пенополиуретан 30 кг / м3, Полиэстерная вата, Нетканый полипропилен',
                'price'=>'5990',
                'category_id'=>'3',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['title'=>'Диван 2-местный Ektorp',
                'alias'=>'divan-ektorp',
                'description'=>'Подушки сиденья с наполнителем высокой упругой пены и полиэфирных ваты обеспечивают удобную поддержку для вашего тела и быстро восстановить свою форму, когда вы встаете. ',
                'price'=>'7990',
                'category_id'=>'3',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['title'=>'Кушетка с 2 матрасами, черный, Мосхульт жесткий',
                'alias'=>'kushetka-moshult',
                'description'=>'Кушетку можно использовать как диван днем и кровать – ночью. Матрас из упругого пенополиуретана обеспечит оптимальную поддержку и комфорт.',
                'price'=>'9490',
                'category_id'=>'6',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['title'=>'Стеллаж 60x180 см. Gersby',
                'alias'=>'stellag-gersby',
                'description'=>'Стеллаж, белый, 60x180 см. Gersby Мы рекомендуем прикреплять мебель к стене с помощью прилагаемого крепежа безопасности. ',
                'price'=>'750',
                'category_id'=>'6',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['title'=>'Комплект столов ВИТШЁ',
                'alias'=>'stoli-vitshe',
                'description'=>'Столешницы из закаленного стекла устойчивы к загрязнениям и просты в уходе.',
                'price'=>'1990',
                'category_id'=>'1',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['title'=>'Диван-кровать 3-местный, Книса',
                'alias'=>'divan-krovat-nisa',
                'description'=>'Покрытие можно снимать и стирать в стиральной машине, что позволяет его легко содржать в чистоте.',
                'price'=>'7990',
                'category_id'=>'3',
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['title'=>'Табурет для ног, Strandmon',
                'alias'=>'taburet-dlya-nog-strandmon',
                'description'=>' Табурет для ног, Нордвалла темно-серый. Strandmon (Артикул: #503.004.16)',
                'price'=>'2000',
                'category_id'=>'6',
                'created_at'=>date('Y-m-d H:i:s')
            ]
        ]);
    }
}
