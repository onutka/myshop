<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name'=>'admin',
                'email'=>'admin@gmail.com',
                'password'=> bcrypt('123123'),
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['name'=>'moder',
                'email'=>'moderator@gmail.com',
                'password'=> bcrypt('321321'),
                'created_at'=>date('Y-m-d H:i:s')
            ],
            ['name'=>'client',
                'email'=>'client@gmail.com',
                'password'=> bcrypt('111111'),
                'created_at'=>date('Y-m-d H:i:s')
            ],
        ]);
    }
}
