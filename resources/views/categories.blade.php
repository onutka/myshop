@extends('template')
@section ('title')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Каталог товаров</h1>
        <p class="lead">Мебель, декор и товары для дома</p>
    </div>
@endsection

@section('good')

    <div class="container">
        <div class="card-deck mb-3 text-center">

                <div class="col-xs-12 col-md-12 col-lg-12">
                    <ul>
                        @foreach($categories as $category)
                        <li class="list-group-item">
                         <a href="/{{$category['alias']}}">
                                <h4 class="my-0 font-weight-normal">{{$category['title']}}</h4>
                        </a>
                        </li>
                        @endforeach
                    </ul>

                    </div>

                </div>
        </div>
    </div>

@endsection