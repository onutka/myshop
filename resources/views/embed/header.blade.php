<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">Almost Ikea Shop</h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="/">Главная</a>
        <a class="p-2 text-dark" href="/categories">Каталог</a>
        <a class="p-2 text-dark" href="/products">Товары</a>
        <a class="p-2 text-dark" href="/pages">Статьи</a>
        <a class="p-2 text-dark" href="/orders">Заказы</a>
        <a class="p-2 text-dark" href="/pages/create">Добавить статью</a>
        <a class="p-2 text-dark" href="/products/create">Добавить товар</a>
        @if (Auth::check())
            <a class="btn btn-outline-primary" href="/pages/create">{{Auth::user()->name}}</a>
            <a class="btn" href="/logout">Выйти</a>
        @else

        <a class="btn btn-outline-primary" href="/login">Войти</a>
        <a class="btn" href="/register">Регистрация</a>

        @endif


    </nav>

</div>