@extends('template')
@section ('title')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Заказы</h1>
        <p class="lead">Информация о пользователях и заказах</p>
    </div>
@endsection

@section('page')

    <div class="container">
        <div class="card-deck mb-3 text-center">

                <table class="table">
                    <tr>
                    <th>Имя пользователя</th>
                    <th>E-mail</th>
                    <th>Телефон</th>
                    <th>Комментарий</th>
                    </tr>
                    @foreach($orders as $order)
                        <tr>
                            <td>{{$order['customer_name']}}</td>
                            <td>{{$order['email']}}</td>
                            <td>{{$order['phone']}}</td>
                            <td>{{$order['feedback']}}</td>
                        </tr>
                    @endforeach
                </table>

@endsection
