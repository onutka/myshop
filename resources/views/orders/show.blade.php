@extends('template')
@section ('title')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto col-lg-8 сol-md-10">
        <h1 class="display-4 text-center">Заказ на имя: {{$order['customer_name']}}</h1>
        <p class="lead text-justify"> Email: {{$order['email']}}</p>
        <p class="lead text-justify"> Телефон: {{$order['phone']}}</p>
        <p class="lead text-justify"> Отзыв: {{$order['feedback']}}</p>
        <a href="/orders">Вернуться ко всем заказам</a>
    </div>
@endsection
