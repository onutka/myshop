@extends('template')
@section ('title')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Статьи про дизайн и декор</h1>
        <p class="lead">Советы по обустройству дома, интерьерные решения и лайфхаки</p>
    </div>
@endsection

@section('page')

    <div class="container">
        <div class="card-deck mb-3 text-center">
           @foreach($pages as $page)
            <div class="col-xs-12 col-md-4 col-lg-4">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">{{$page['title']}}</h4>
                </div>
                <div class="card-body">

                    <p>{{$page['intro']}}</p>
                    <a href="/pages/{{$page['alias']}}/" type="button" class="btn btn-lg btn-block btn-primary">Подробнее</a>
                    <a href="/pages/{{$page['alias']}}/edit">Редактировать</a>
                    <a href="/pages/{{$page['alias']}}/delete" class="btn-danger">Удалить</a>
                </div>
            </div>
            @endforeach
@endsection




