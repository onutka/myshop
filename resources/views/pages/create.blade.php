@extends('template')

@section('title')
    <h1>Добавление новой статьи</h1>
@endsection
@section('page')
    <div class="col-lg10 col md-12">
        <form action="/pages" method="post" class="form-horizontal">
            <div class="alert alert-danger">

                <ul> @foreach($errors->all() as $error)

                    <li>{{$error}}</li>
                    @endforeach

                </ul>
            </div>
            {{csrf_field()}}
            <div class="form-group">
                <label for="title">Название статьи</label>
                <input type="text" name="title" id="title" class="form-control">
            </div>
            <div class="form-group">
                <label for="alias">Alias</label>
                <input type="text" name="alias" id="alias" class="form-control">
            </div>
            <div class="form-group">
                <label for="intro">Вступление</label>
                <textarea name="intro" id="intro" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <label for="oontent">Cодержание</label>
                <textarea name="content" id="content" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <button class="btn-lg btn-primary">Создать</button>
            </div>
        </form>
    </div>


@endsection