@extends('template')

@section('title')
    <h1>Редактирование статьи</h1>
@endsection
@section('page')
    <div class="col-lg10 col md-12">
        <form action="/pages/{{$page->alias}}" method="post" class="form-horizontal">

            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <div class="form-group">
                <h3>Вы действительно хотите удалить статью {{$page->title}} ?</h3>
            </div>

            <div class="form-group">
                <button class="btn-lg btn-danger">Удалить</button>
            </div>
        </form>
    </div>


@endsection