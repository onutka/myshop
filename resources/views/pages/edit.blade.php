@extends('template')

@section('title')
    <h1>Редактирование статьи</h1>
@endsection
@section('page')
    <div class="col-lg10 col md-12">
        <form action="/pages/{{$page->alias}}" method="post" class="form-horizontal">
            <div class="alert alert-danger">

                <ul> @foreach($errors->all() as $error)

                    <li>{{$error}}</li>
                    @endforeach

                </ul>
            </div>

            {{csrf_field()}}
            <input type="hidden" name="_method" value="PATCH">
            <div class="form-group">
                <label for="title">Название статьи</label>
                <input value="{{$page->title}}" type="text" name="title" id="title" class="form-control">
            </div>
            <div class="form-group">
                <label for="alias">Alias</label>
                <input value="{{$page->alias}}" type="text" name="alias" id="alias" class="form-control">
            </div>
            <div class="form-group">
                <label for="intro">Вступление</label>
                <textarea name="intro" id="intro" class="form-control">{{$page->intro}}</textarea>
            </div>
            <div class="form-group">
                <label for="oontent">Cодержание</label>
                <textarea name="content" id="content" class="form-control">{{$page->content}}</textarea>
            </div>
            <div class="form-group">
                <button class="btn-lg btn-primary">Редактировать</button>
            </div>
        </form>
    </div>


@endsection