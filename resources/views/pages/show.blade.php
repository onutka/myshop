@extends('template')
@section ('title')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto col-lg-8 сol-md-10">
        <h1 class="display-4 text-center">{{$page['title']}}</h1>
        <p class="lead text-center"> {{$page['intro']}}</p>
        <p class="lead text-justify"> {{$page['content']}}</p>
        <a href="/pages">Вернуться к статьям</a>
    </div>
@endsection
