@extends('template')
@section ('title')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Каталог товаров</h1>
        <p class="lead">Мебель, декор и товары для дома</p>
    </div>
@endsection

@section('good')

    <div class="container">
        <div class="card-deck mb-3 text-center">
            @foreach($products as $product)
                <div class="col-xs-12 col-md-4 col-lg-4">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">{{$product['title']}}</h4>
                    </div>
                    <h1 class="card-title pricing-card-title">{{$product['price']}}<small class="text-muted"> грн.</small></h1>
                    <div class="card-body">

                        <p>{{$product['description']}}</p>
                       <a href="/products/{{$product['alias']}}">Подробнее</a>
                        <a href="/products/{{$product['alias']}}/edit">Редактировать</a>
                        <a href="/products/{{$product['alias']}}/delete" class="btn-danger">Удалить</a>
                        <a href="#" type="button" class="btn btn-lg btn-block btn-primary">Купить</a>

                    </div>
                </div>
    @endforeach
@endsection
