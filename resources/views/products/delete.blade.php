@extends('template')

@section('title')
    <h1>Удаление товара</h1>
@endsection
@section('page')
    <div class="col-lg10 col md-12">
        <form action="/products/{{$product->alias}}" method="post" class="form-horizontal">
            <div class="alert alert-danger">

                <ul> @foreach($errors->all() as $error)

                        <li>{{$error}}</li>
                    @endforeach

                </ul>
            </div>
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <div class="form-group">
                <h3>Вы действительно хотите удалить товар {{$product->title}} ?</h3>
            </div>

            <div class="form-group">
                <button class="btn-lg btn-primary">Удалить</button>
            </div>
        </form>
    </div>
@endsection