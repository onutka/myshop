@extends('template')

@section('title')
    <h1>Редактирование товара</h1>
@endsection
@section('page')
    <div class="col-lg10 col md-12">
        <form action="/products/{{$product->alias}}" method="post" class="form-horizontal">
            <div class="alert alert-danger">

                <ul> @foreach($errors->all() as $error)

                        <li>{{$error}}</li>
                    @endforeach

                </ul>
            </div>
            {{csrf_field()}}
            <input type="hidden" name="_method" value="PATCH">
            <div class="form-group">
                <label for="title">Название товара</label>
                <input value="{{$product->title}}" type="text" name="title" id="title" class="form-control">
            </div>
            <div class="form-group">
                <label for="alias">Alias</label>
                <input value="{{$product->alias}}" type="text" name="alias" id="alias" class="form-control">
            </div>
            <div class="form-group">
                <label for="description">Описание</label>
                <textarea name="description" id="description" class="form-control">{{$product->description}}</textarea>
            </div>
            <div class="form-group">
                <label for="price">Цена</label>
                <input value="{{$product->price}}" type="text" name="price" id="price" class="form-control">
            </div>
            <div class="form-group">
                <button class="btn-lg btn-primary">Редактировать</button>
            </div>
        </form>
    </div>
@endsection