@extends('template')
@section ('title')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto col-lg-8 сol-md-10">
        <h1 class="display-4 text-center">{{$product['title']}}</h1>
        <p class="lead text-justify"> {{$product['description']}}</p>
        <p class="lead text-center"><h2>Цена: {{$product['price']}} грн.</h2></p>
        <p> <a href="#" class="btn-primary btn-lg">Купить</a></p>
        <a href="/products">Вернуться к товарам</a>
    </div>
@endsection
