@extends('template');
@section('good');
    <div class="col md-8 col-lg-6 col-xs-12 col-sm-12">
        <form method ="POST" action="/sessions">
            {{csrf_field()}}
            @include('embed.errors')
            <div class="form-group" >
                <label for="email">E-mail</label>
                <input type="email" class="form-control" name="email" id="email">
            </div>
            <div class="form-group">
                <label for="password">Пароль</label>
                <input type="password" class="form-control" name="password" id="password">
            </div>
            <div class="form-group">
                <button class="btn btn-outline-primary">Войти</button>
            </div>
        </form>
    </div>
    @endsection;