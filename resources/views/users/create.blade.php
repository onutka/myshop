@extends('template');
@section('good');
<div class="col md-8 col-lg-6 col-xs-12 col-sm-12">
    <h1>Регистрация</h1>
    <form method ="POST" action="/register">
        {{csrf_field()}}
        @include('embed.errors')
        <div class="form-group" >
            <label for="name">Имя</label>
            <input type="text" class="form-control" name="name" id="name">
        </div>
        <div class="form-group" >
            <label for="email">E-mail</label>
            <input type="email" class="form-control" name="email" id="email">
        </div>
        <div class="form-group">
            <label for="password">Пароль</label>
            <input type="password" class="form-control" name="password" id="password">
        </div>
        <div class="form-group">
            <label for="password_confirmation">Повторите пароль</label>
            <input type="password" class="form-control" name="password_сonfirmation" id="password_confirmation">
        </div>
        <div class="form-group">
            <button class="btn btn-outline-primary" type="submit">Зарегистрироваться</button>
        </div>
    </form>
</div>
@endsection;