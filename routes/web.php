<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('main');
});*/
Route::get('/products/{product}/delete', 'ProductsController@delete');
Route::get('/pages/{page}/delete', 'PagesController@delete');
Route::get('/orders/{id}', 'OrdersController@show');
Route::get('/orders', 'OrdersController@orders');
Route::get('/', 'HomeController@home');
Route::get('/pages', 'PagesController@pages');
Route::get('/products', 'ProductsController@products');
Route::resources([
    'products'=>'ProductsController',
    'pages'=>'PagesController',
]);
Route::get('/stoli', 'CategoriesController@show');
Route::get('/login', 'SessionsController@create')->name('login');
Route::post('/sessions', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');
Route::get('/register', 'RegistrationController@create');
Route::post ('/register', 'RegistrationController@store');
Route::get('/categories', 'CategoriesController@index');

/*Route::get('/pages/create', 'PagesController@create');
Route::get('/pages/{page}', 'PagesController@show');
Route::get('/pages/{page}/edit', 'PagesController@edit');
Route::post('/pages', 'PagesController@store');
Route::patch('/pages/{page}', 'PagesController@update');
Route::delete('/pages/{page}', 'PagesController@destroy');

Route::get('/products/create', 'ProductsController@create');
Route::get('/products/{product}/edit', 'ProductsController@edit');
Route::get('/products/{product}', 'ProductsController@show');
Route::patch('/products/{product}', 'ProductsController@update');
Route::post('/products', 'ProductsController@store');
Route::delete('/products/{product}', 'ProductsController@destroy');*/


/*Route::post('/pages/{page}/destroy', 'PagesController@destroy'); вариант 1 без использ метода delete*/

//delete.blade добавляем скрытое поле

/*Route::post('/products/{product}/destroy', 'ProductsController@destroy');*/
